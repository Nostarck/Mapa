#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funciones.h"
#define N 21
#define INF 9999

char** ciudades;



/**
 * @brief main
 * funcion principal del programa
 */
int main(){
    
    cargar_datos();
    
    int opcion;
    do{
        system("clear");
        printf("Algoritmos de grafos para el mapa de juego de tronos");
        printf("\n\nCual algoritmo desea utilizar?:\n");
        printf("(1) Floyd ");
        printf("(2) Dijkstra ");
        printf("(3) Prim ");
        printf("(4) Salir \n");
        scanf("%i",&opcion);
    
        switch(opcion){
            case 1: floyd(); break;
            case 2: dijkstra(); break;
            case 3: prim(); break;
            case 4: system("clear"); printf("\nHasta luego, vuelva pronto!"); break;
        }
    }while(opcion != 4);
    return 0;
}

/**
 * @brief carga los archivos
 * carga 2 archivos, una con el grafo del mapa y uno con los nombres
 * de las ciudades
 * @return void
 */
void cargar_datos(){
    
    char* linea_datos = (char*)calloc(1000,sizeof(char));
    char* nombreCiudad = (char*)calloc(1000,sizeof(char));
    
    ciudades = (char**)malloc(sizeof(char**) * N);
    for(int i = 0; i < N; i++){
        ciudades[i] = (char*)malloc(sizeof(char) *  N);
    }
    
    FILE *f,*fc;
    f = fopen("matArreglada.txt","r");
    fc = fopen("ciudades.txt","r");
    
    int i = 0;
    while(i < N){
        fgets(linea_datos,1000,f);
        fgets(nombreCiudad,N,fc);
       
        ciudades[i] = strcpy(ciudades[i],nombreCiudad);
        ciudades[i][strlen(ciudades[i])-1] = '\0';
        grafo[i][0] = atoi(strtok(linea_datos," "));
        for(int j = 1; j < N; j++){
            grafo[i][j] = atoi(strtok(NULL," "));
        }
        
        i++;
    }
    
}

/**
 * @brief pide una ciudad
 * @param char* mensaje
 * recibe un string con la peticion del programa
 * y el usuario responde
 * @return int
 */
int pedir_ciudad(char* mensaje){
    
    int opcion;
    system("clear");
    printf("%s:\n",mensaje);
    for(int i = 0; i < N; i++){
        printf("(%i) %s\n",i,ciudades[i]);
    }
    printf("opcion: ");
    scanf("%i",&opcion);
    printf("\n");
    
    return opcion;
}

/**
 * @brief metodo que estima el mejor recorrido de un grafo mediante el algoritmo de Floyd
 * 
 * Este metodo se encarga de obtener los mejores caminos (con menos muertos) desde un punto dado
 * hasta otro mediante el algoritmo de floyd. Para su resolucion, el metodo utiliza una matriz con 
 * los valores de los muertos y otra matriz vacia a la que llena con los mismos datos de la primera
 * matriz. Una vez agregados los datos recorre completamente la matriz N veces para ir calculando
 * los mejores caminos para cada ciudad.
 * 
 * @return void 
 */
void floyd(){
	int origen, destino;
	
	origen  = pedir_ciudad("Escoje una ciudad de ORIGEN");
    destino = pedir_ciudad("Ahora, escoje una ciudad de DESTINO");
	
    int matriz[N][N];
 
    for (int i = 0; i < N; i++){
        
        for (int j = 0; j < N; j++){
            
            matriz[i][j] = grafo[i][j];
        
		}
	}
    
    for (int i = 0; i < N; i++){
        
        for (int j = 0; j < N; j++){
            
            for (int k = 0; k < N; k++){

                if (matriz[j][i] + matriz[i][k] < matriz[j][k]){
                    matriz[j][k] = matriz[j][i] + matriz[i][k];
				}
            }
        }
    }
    
    system("clear");
	printf("El camino más seguro desde %s hasta %s presenta %d muertos. Suerte en tu viaje.\n", ciudades[origen], ciudades[destino], matriz[origen][destino]);
	printf("\nPresiona enter para continuar\n");
    getchar(); getchar();
}

/**
 * @brief imprime dijkstra
 * imprime la respuesta del algoritmo dijkstra
 * @param int pesos[N][N]
 * @param int origen
 * 
 * @return void
 * 
 */
void imprimirDijkstra(int pesos[N][N],int origen){
    printf("Desde %s las muertes a cada ciudad son de:",ciudades[origen]);
    printf("\nCiudades   -----> muertos\n");
    for(int j = 0; j < N; j++){
        printf("\n%s      ----->  %i",ciudades[j],pesos[20][j]);
    }
    getchar(); getchar();
    
}



/**
 * @brief algoritmo dijkstra
 * realiza el algoritmo dijkstra paa calcular los caminos mas cortos
 * de la ciudad escogida
 * @return void
 */
void dijkstra(){
    int origen;
    
    origen  = pedir_ciudad("Escoje una ciudad de ORIGEN");
    
    int pesos[N][N];
    int visitas[N];
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            pesos[i][j] = INF;
        }
        visitas[i] = 0;
    }
    
    int actual = origen;
    pesos[0][actual] = 0;
    visitas[actual] = 1;
    for(int i = 1; i < N; i++){
        
        for(int k = 0; k < N; k++){
            if(pesos[i-1][actual]+grafo[actual][k] < pesos[i-1][k]){
                pesos[i][k] = pesos[i-1][actual]+grafo[actual][k];
            }
            else{
                pesos[i][k] = pesos[i-1][k];
            }
        }
        actual = min_dij(actual,visitas,pesos[i]);
        visitas[actual] = 1;
    }
    system("clear");
    imprimirDijkstra(pesos,origen);
}

/**
 * @brief busca el menor de dijkstra
 * @param int act
 * @param int visitas[N]
 * @param int fila_pesos[N]
 * 
 * busca el menor de la fila del cuadro de dijkstra
 * para saber cual nodo sigue por calcular
 * @return int
 */
int min_dij(int act, int visitas[N], int fila_pesos[N]){
    
    int menor = INF;
    int nuevo_act = act;
    for(int i = 0; i < N; i++){
        if(fila_pesos[i] < menor && visitas[i] == 0){
            menor = fila_pesos[i];
            nuevo_act = i;
        }
    }
    return nuevo_act;
}


/**
 * @brief imprime el resultado de obtener los caminos mas cortos a traves del algoritmo de prim
 * 
 * @param int[]
 * @param int[][]
 * 
 * Este metodo se encarga de imprimir el resultado obtenido de calcular los caminos mas cortos hacia 
 * cada ciudad a traves del algoritmo de prim. Se imprime en un orden sencillo donde la primera ciudad 
 * mostrada es la ciudad de donde se proviene, la segunda es la ciudad a donde se llega y el numero a la
 * derecha es la cantidad de muertos que se encuentran en el camino.
 * 
 * @return void
 */
void imprimirPrim(int arbol[], int matriz[N][N]){
	printf("\tRuta\t\t\tCantidad de Muertos\n");
	for(int i = 1; i < N; i++){
		if(arbol[i] != -1){
			printf("%s - %s\t\t\t%d\n", ciudades[arbol[i]], ciudades[i], matriz[arbol[i]][i]);
		}
	}
	
	printf("\nPresiona enter para continuar\n");
    getchar(); getchar();
}

/**
 * @brief metodo que retorna el numero mas pequeno del vector de valores 
 * 
 * @param int[]
 * @param int[]
 * 
 * Este metodo se encarga de examinar el vector de valores y encontrar cual es el numero mas bajo
 * y tambien examinando si para la misma posicion del vector visitados, este no ha sido utilizado 
 * 
 * @return int
 */

int posMinimo(int valores[], int visitados[]){
	int min = INF; 
	int pos;
	
	for(int i = 0; i < N; i++){
		if(visitados[i] == 0 && valores[i] < min){
			min = valores[i];
			pos = i;			
		}
	}
	return pos;
}


/**
 * @brief calcula el camino mas corto hacia cada ciudad a traves del algoritmo de Prim
 * 
 * Este metodo se encarga de crear un arbol de expansion de coste minimo, desde una ciudad dada,
 * a partir del cual se obtienen los caminos mas cortos hacia cada ciudad. El algoritmo trabaja con
 * arreglos que constantemente actualiza cuando encuentra caminos mas cortos.
 * 
 * @return void
 */
void prim(){
	int origen;
    origen  = pedir_ciudad("Escoje una ciudad de ORIGEN");
	
	int arbol[N]; 
	int valores[N]; 
	int visitados[N]; 
	
	for(int i = 0; i < N; i++){
		valores[i] = INF;
		visitados[i] = 0;
		
	}

	valores[origen] = 0;
	arbol[origen] = -1;
	
	for(int i = 0; i < N-1; i++){
		int pos = posMinimo(valores, visitados);
		
		visitados[pos] = 1;
		
		for(int j = 0; j < N; j++){
			if(grafo[pos][j] != 0 && visitados[j] == 0 && grafo[pos][j] < valores[j]){
				arbol[j] = pos; 
				valores[j] = grafo[pos][j];
			}
		}
		
	}
	imprimirPrim(arbol, grafo);
}

