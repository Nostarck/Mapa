#ifndef HEADER_FILE
#define HEADER_FILE
#define N 21
void floyd();
void imprimirFloyd(int matriz[N][N]);
void dijkstra();
int pedir_ciudad(char* mensaje);
char* limpiar(char* str);
void cargar_datos();
int min_dij(int act, int visitas[N], int fila_pesos[N]);
int grafo[N][N];
int posMinimo(int valores[], int visitados[]);
void imprimirPrim(int arbol[], int matriz[N][N]);
void prim();
#endif
